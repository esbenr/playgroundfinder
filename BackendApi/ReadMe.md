Backend Api
==============

This is the folder for the backend and api.

Guidelines:
Don't use PHP, use a technology that scales and is maintainable. (Etc. Asp.NET/C#)

Persistance:
Geo coordinates should be searchable (Do not use number or strings, but actual geo coordinates)
Fint solution for binary (image-) data. I.e. Amazon S3 or so.
Maybe use MS Azure for hosting.
